# Super Panda World

2D sidescroller Mario clone with a Panda as the main character

### High Concept

The sacred gems have been stolen and many more scattered throughout the land. Our hero must seek out and return as many gems as possible to the sanctuary. The mastermind behind the theft has managed to steal the star opal, a one of a kind stone entrusted to the keepers for safeguarding against foul use of it's hidden powers.

## GamePlay
![Main Menu Demo](/Screenshots/main-menu-demo-01-26.png)

![Game Over](/Screenshots/Game-Over-01-26.png)

![Level 1 Layout](/Screenshots/Level1-Layout-01-29.png)

# Overview

Reach the end of each level by jumping over various gaps and avoiding enemies along the way. Enemies include monkeys, snakes, and beetles. All enemies can be defeated when our main hero jumps on top of them. Some enemies can only be defeated with power up items which can be obtained from breaking various blocks scattered throughout the land. Other enemies must be avoided entirely.

## Main Objective

Get to the end of each level before timer runs out

## Gameplay Mechanics

* Jump
* Super Jump
* Fire projectile

## Development Information

* Unity Version Used: 5.5.0xf3
* Development Platform: Gentoo Linux

## Platform Testing

* Project works and behaves as expected when opened in OSX on Mac - confirmed
* Project works and behaves as expected when opened in Windows - confirmed
* Project works and behaves as expected when opened in various Linux distros - confirmed

## Running the Project locally

1. Clone the repository using git
2. Open Unity5.5 on any supported operating system
3. Select the option to open a project in Unity
4. Navigate to the project folder pulled in by git and open it
5. Wait for everything to import then click the 'Play' button to test the game in the engine environment.

More information to come soon.
