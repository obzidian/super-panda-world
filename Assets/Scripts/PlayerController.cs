﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	// Player Movement Speed
	public float moveSpeed;

	// Private rigid body variable to store copy of player rigid body
	private Rigidbody2D myRigidbody;

	// Player jump speed
	public float jumpSpeed;

	// Variable for checking if player is in the air
	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;

	public bool isGrounded;

	// variable for player respawning 
	public Vector3 respawnPosition;

	public LevelManager theLevelManager;

	// Use this for initialization
	void Start () {
		
		// get the rigidbody component that's attached to the player
		myRigidbody = GetComponent<Rigidbody2D>();

		//default respawn point at player starting position
		respawnPosition = transform.position; 

		//find the level manager in the world
		theLevelManager = FindObjectOfType<LevelManager> ();
	}
	
	// Update is called once per frame
	void Update () {

		isGrounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);

		if (Input.GetAxisRaw ("Horizontal") > 0f) {
			myRigidbody.velocity = new Vector3 (moveSpeed, myRigidbody.velocity.y, 0f);
		} else if (Input.GetAxisRaw ("Horizontal") < 0f) {
			myRigidbody.velocity = new Vector3 (-moveSpeed, myRigidbody.velocity.y, 0f);
		} else {
			myRigidbody.velocity = new Vector3 (0f, myRigidbody.velocity.y, 0f);
		}

		if (Input.GetButtonDown ("Jump") && isGrounded) {
			myRigidbody.velocity = new Vector3 (myRigidbody.velocity.x, jumpSpeed, 0f);
		}

		// animation code will go here in the future
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "KillPlane") {
			theLevelManager.Respawn();
		}

		if (other.tag == "Checkpoint") {
			respawnPosition = other.transform.position;
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "MovingPlatform") {
			transform.parent = other.transform;
		}
	}

	void OnCollisionExit2D(Collision2D other)
	{
		if (other.gameObject.tag == "MovingPlatform") {
			transform.parent = null;
		}
	}
}
