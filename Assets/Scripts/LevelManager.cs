﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

	// variable to implement respawn timer after death
	public float waitToRespawn;
	public PlayerController thePlayer;

	// variable for death explosion effect
	public GameObject deathEffect;

	// variable for coin count on HUD by level manager
	public int coinCount;

	public Text coinText;

	public Text livesText;
	public int startingLives;
	public int currentLives;

	public GameObject gameOverScreen;

	// Use this for initialization
	void Start () {
		thePlayer = FindObjectOfType<PlayerController> ();
		coinText.text = ": " + coinCount;
		currentLives = startingLives;
		livesText.text = "Lives x " + currentLives;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Respawn()
	{
		currentLives -= 1;
		livesText.text = "Lives X " + currentLives;

		if (currentLives > 0) {
			StartCoroutine ("RespawnCo");
		} else {
			thePlayer.gameObject.SetActive (false);
			gameOverScreen.SetActive (true);
		}
	}

	public IEnumerator RespawnCo()
	{
		// kill player by deactivating
		thePlayer.gameObject.SetActive (false);

		Instantiate(deathEffect, thePlayer.transform.position, thePlayer.transform.rotation);

		// pause the game for a moment before respawning
		yield return new WaitForSeconds (waitToRespawn);

		// grab the respawn location from player controller and move player position to it
		thePlayer.transform.position = thePlayer.respawnPosition;
		thePlayer.gameObject.SetActive (true);
	}

	public void AddCoins(int coinsToAdd)
	{
		coinCount += coinsToAdd;
		coinText.text = ": " + coinCount;

	}

	public void AddLives(int livesToAdd)
	{
		currentLives += livesToAdd;
		livesText.text = "Lives X " + currentLives;
	}
}
