﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	// variable holder for the player
	public GameObject target;

	// variable for keeping camera view in front of the player
	public float followAhead;

	private Vector3 targetPosition;

	public float smoothing;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		targetPosition = new Vector3 (target.transform.position.x, transform.position.y, transform.position.z);

		// moves the camera to a predetermined target ahead of the player
		if (target.transform.localScale.x > 0f) 
		{
			targetPosition = new Vector3 (targetPosition.x + followAhead, targetPosition.y, targetPosition.z);
		} else {
			targetPosition = new Vector3 (targetPosition.x - followAhead, targetPosition.y, targetPosition.z);
		}

		transform.position = Vector3.Lerp(transform.position, targetPosition, smoothing * Time.deltaTime);
	}
}
