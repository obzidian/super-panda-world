﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

	private LevelManager theLevelManager;

	public int coinValue;

	// Use this for initialization
	void Start () {
		theLevelManager = FindObjectOfType<LevelManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		theLevelManager.AddCoins (coinValue);

		if (other.tag == "Player") {
			Destroy (gameObject);
		}
	}
}
